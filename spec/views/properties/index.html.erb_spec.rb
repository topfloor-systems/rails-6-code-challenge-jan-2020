# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'properties/index', type: :view do
  it 'displays all Properties' do
    properties = create_list(:property, 2)
    assign(:properties, properties)

    render

    expect(rendered).to include 'Properties'

    properties.each do |property|
      expect(rendered).to include property.property_name
    end
  end
end
